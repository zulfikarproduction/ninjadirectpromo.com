import styled from 'styled-components';
// import image1 from '../../images/bg1@2x.png'

export const Title = styled.h1`
  color:#C0022D;
  text-align: center;
  padding-left: 10vw;
  padding-right: 10vw;
  font-style: bold;

  @media screen and (max-width:960px){

    font-size: 1.6rem;
  }


`

export const Paragraph = styled.p`
  max-width: 960px;
  font-size: 1rem;
  text-align: center;
  margin: auto;

  @media screen and(max-width:960px){
    font-size: 2px;
  }
`